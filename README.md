
# Docker Usage Steps
## About

in this guide you'll be able to run the shopApplication project using Docker
## Prerequisites

To run this project, you will need to have Docker installed on your computer. If you don't have it installed, you can download it from the Docker website.
## Usage

To run this project, follow these steps:

Clone the repository to your local machine:

bash

    git clone https://gitlab.com/abdelwahed.abbad/imedia24-coding-challenge

Navigate to the root directory of the project:

bash

    cd imedia24-coding-challenge

Build the Docker image:

     docker build -t imedia24-coding-challenge .   .

Run the Docker container:

    docker run -p 8080:8080 imedia24-coding-challenge   

Access the application in your browser:

http://localhost:8080

# iMedia24 Coding challenge

### Reference Documentation
For further reference, please consider the following sections:

* [Official Gradle documentation](https://docs.gradle.org)
* [Official Kotlin documentation](https://kotlinlang.org/docs/home.html)
* [Spring Boot Gradle Plugin Reference Guide](https://docs.spring.io/spring-boot/docs/2.4.3/gradle-plugin/reference/html/)
* [Create an OCI image](https://docs.spring.io/spring-boot/docs/2.4.3/gradle-plugin/reference/html/#build-image)
* [Flyway database migration tool](https://flywaydb.org/documentation/)

### Additional Links
These additional references should also help you:

* [Gradle Build Scans – insights for your project's build](https://scans.gradle.com#gradle)

