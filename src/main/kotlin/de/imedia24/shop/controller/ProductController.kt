package de.imedia24.shop.controller

import de.imedia24.shop.db.entity.ProductEntity
import de.imedia24.shop.domain.product.ProductRequest
import de.imedia24.shop.domain.product.ProductResponse
import de.imedia24.shop.service.ProductService
import org.slf4j.LoggerFactory
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import java.time.ZonedDateTime
import javax.websocket.server.PathParam

@RestController
class ProductController(private val productService: ProductService) {

    private val logger = LoggerFactory.getLogger(ProductController::class.java)!!

    @GetMapping("/product/{sku}", produces = ["application/json"])
    fun findProductsBySku(
        @PathVariable("sku") sku: String
    ): ResponseEntity<ProductResponse> {
        logger.info("Request for product $sku")

        val product = productService.findProductBySku(sku)
        return if(product == null) {
            ResponseEntity.notFound().build()
        } else {
            ResponseEntity.ok(product)
        }
    }
    @GetMapping("/products", produces = ["application/json"])
    fun getProductsBySkus(@RequestParam skus: List<String>): ResponseEntity<List<ProductResponse>> {
        logger.info("Request for product list information $skus")
        val productsList=productService.findProductsBySkus(skus)
        print(productsList)
        return if(productsList == null) {
            ResponseEntity.notFound().build()
        } else {
            ResponseEntity.ok(productsList)
        }
    }
    @PostMapping("product",produces = ["application/json"])
    fun addProduct(@RequestBody request:ProductRequest) :ResponseEntity<ProductResponse> {
        logger.info("Adding a new product $request")
        val productToAdd=ProductEntity(request.sku,request.name,request.description,request.price,request.stock,
            ZonedDateTime.now(),ZonedDateTime.now())
        val addedProduct= productService.addProduct(productToAdd)
        return ResponseEntity.status(HttpStatus.CREATED)
            .contentType(MediaType.APPLICATION_JSON)
            .body(addedProduct)
    }



    @PatchMapping("product/{sku}")
    fun updateProduct(@PathVariable sku: String, @RequestBody request: ProductRequest):ResponseEntity<ProductResponse>{
        logger.info("Updating a product $request")
        val productToUpdate=ProductEntity(request.sku,request.name,request.description,request.price,request.stock,
            ZonedDateTime.now(),ZonedDateTime.now())
        val updatedProduct = productService.partialUpdateProduct(sku, productToUpdate)
        return ResponseEntity.status(HttpStatus.OK).body(updatedProduct)
    }
}
