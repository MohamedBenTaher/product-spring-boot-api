package de.imedia24.shop.db.entity

import io.swagger.v3.oas.annotations.media.Schema
import org.hibernate.annotations.UpdateTimestamp
import java.math.BigDecimal
import java.time.ZonedDateTime
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.Table

@Entity
@Table(name = "products")
data class ProductEntity(
    @field:Schema(
        description = "Sku",
        example = "1234",
        type = "String",
    )
    @Id
    @Column(name = "sku", nullable = false)
    val sku: String,


    @field:Schema(
        description = "Name",
        example = "product",
        type = "String",
    )
    @Column(name = "name", nullable = false)
    var name: String,


    @field:Schema(
        description = "Description",
        example = "product",
        type = "String",
    )
    @Column(name = "description")
    var description: String? = null,


    @field:Schema(
        description = "Price",
        example = "20006",
        type = "Decimal",
    )
    @Column(name = "price", nullable = false)
    var price: BigDecimal,


    @field:Schema(
        description = "Stock",
        example = "16",
        type = "Int",
    )
    @Column(name = "stock", nullable = false, )
    val stock: Int, // new field for stock information


    @field:Schema(
        description = "created AT",
        example = "2022-10-31T09:00:00.594Z",
        type = "Date",
    )
    @UpdateTimestamp
    @Column(name = "created_at", nullable = false)
    val createdAt: ZonedDateTime= ZonedDateTime.now(),


    @field:Schema(
        description = "updated AT",
        example = "2022-10-31T09:00:00.594Z",
        type = "Date",
    )
    @UpdateTimestamp
    @Column(name = "updated_at", nullable = false)
    var updatedAt: ZonedDateTime= ZonedDateTime.now()


){
    constructor() : this("", "", null, BigDecimal.ZERO,  0, ZonedDateTime.now(),ZonedDateTime.now())}

