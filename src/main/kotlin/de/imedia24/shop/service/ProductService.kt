package de.imedia24.shop.service

import de.imedia24.shop.db.entity.ProductEntity
import de.imedia24.shop.db.repository.ProductRepository
import de.imedia24.shop.domain.product.ProductResponse
import de.imedia24.shop.domain.product.ProductResponse.Companion.toProductResponse
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Service
import org.springframework.web.server.ResponseStatusException
import java.time.ZonedDateTime

@Service
class ProductService(private val productRepository: ProductRepository) {

    fun findProductBySku(sku: String): ProductResponse? {
        val productEntity = productRepository.findBySku(sku)
            ?: throw ResponseStatusException(HttpStatus.NOT_FOUND, "Product not found")

        return productEntity.toProductResponse()
    }

    fun findProductsBySkus(skus : List<String>):List<ProductResponse>?{
        val productEntities = productRepository.findBySkuIn(skus)
        val products = productEntities.map { it.toProductResponse() }
        return products
    }

    fun addProduct(request: ProductEntity):ProductResponse{
        val addedProduct=productRepository.save(request)
        return addedProduct.toProductResponse();
    }

    fun partialUpdateProduct(sku:String,request:ProductEntity): ProductResponse? {
        var productToUpdate = productRepository.findBySku(sku)
            ?: throw ResponseStatusException(HttpStatus.NOT_FOUND, "Product not found")

        request.name?.let { productToUpdate.name = it }
        request.description?.let { productToUpdate.description = it }
        request.price?.let { productToUpdate.price = it }
        request.updatedAt= ZonedDateTime.now()
        val updatedProduct = productRepository.save(productToUpdate)
        return updatedProduct.toProductResponse()
    }

}
